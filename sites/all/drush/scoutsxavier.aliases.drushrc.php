<?php


$aliases['dev'] = array(
    'root'          => '/Applications/MAMP/htdocs/scoutsxavier',
);

$aliases['prod'] = array (
  'root' => '/var/www/vhosts/scoutsxavier.es/httpdocs',
  'remote-host'   => 'scoutsxavier.es',
  'remote-user'   => 'xavier',
  'ssh-options'  => '-p 22022',
);

