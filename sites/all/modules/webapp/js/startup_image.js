// this variable is used later from webapp.js to restore
// the original viewport once the dom is loaded
var webapp_original_viewport = false;

(function (w, d){
    if (typeof(WEBAPP_STARTUP) === "undefined"){
        return;
    }
    var
        nav = w.navigator,
        gfxpath = WEBAPP_STARTUP.startup_root;
        cache_suffix = '?' + WEBAPP_STARTUP.cache_token;
        pixelRatio = w.devicePixelRatio === 2 ? '@2x' : '',
        device = (/ipad/i).test(nav.platform) ? 'ipad' : (/iphone|ipod/i).test(nav.platform) ? 'iphone' : '',
        orient = device == 'ipad' && w.orientation % 180 ? 'landscape' : 'portrait';
    if(device == 'iphone' && w.screen.height == 568) {
        device = 'iphone5';
        webapp_original_viewport = d.querySelector("meta[name=viewport]").content;
        d.querySelector("meta[name=viewport]").content="width=320.1";
    }
    if (device === ''){
        return;
    }
    if (device == 'iphone' || device == 'iphone5' || nav.standalone){
        d.write('<link rel="apple-touch-startup-image" href="' + gfxpath + 'startup-' + device + '-' + orient + pixelRatio + '.png' + cache_suffix + '"' + (device == 'ipad' ? ' media="screen and (orientation:' + orient + ')"' : '') + '>');
    } else{
        d.write('<link rel="apple-touch-startup-image" href="' + gfxpath + 'startup-' + device + '-landscape' + pixelRatio + '.png' + cache_suffix + '" media="screen and (orientation:landscape)">');
        d.write('<link rel="apple-touch-startup-image" href="' + gfxpath + 'startup-' + device + '-portrait' + pixelRatio + '.png' + cache_suffix + '" media="screen and (orientation:portrait)">');
    }
})(window, document);